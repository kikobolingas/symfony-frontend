import {Controller} from "stimulus";
import Swal from "sweetalert2";

export default class extends Controller {
    connect() {
        console.log("its working!!");
    }

    confirm(e) {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            showCancelButton: true,
        }).then((result) => {
            if (result.isConfirmed) {
                this.element.submit();
                /*Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )*/
            }
        })
    }
}
