<?php

namespace App\Controller;

use App\Entity\Talk;
use App\Form\TalkType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TalksController extends AbstractController
{
    private string $talksImagePath;

    public function __construct(string $talksImagePath)
    {
        $this->talksImagePath = $talksImagePath;
    }

    #[Route('/info', name: 'info')]
    public function info(Request $request): Response
    {
        if ($request->getMethod() === Request::METHOD_POST) {
            dd($request->request->get('title'));
        }

        return $this->render('contact/contact-form-html.twig');
    }

    #[Route('/', name: 'homepage')]
    public function listTalks(EntityManagerInterface $entityManager): Response
    {
        $talks = $entityManager->getRepository(Talk::class)->findAll();
        return $this->render('talks/list.html.twig',
            [
                'talks' => $talks
            ]
        );
    }

    #[Route('/talks/new', name: 'new_talk')]
    public function newTalk(Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(TalkType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {/* @var $avatar UploadedFile */

            /** @var Talk $talk */
            $talk = $form->getData();

            /** @var UploadedFile $image */
            $image = $form->get('image')->getData();

            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = $originalFilename.'-'.uniqid().'.'.$image->guessExtension();
                $image->move($this->talksImagePath, $newFilename);
                $talk->setImage($newFilename);
            }

            $entityManager->persist($talk);
            $entityManager->flush();

            return $this->redirectToRoute('homepage');

            //dd($talk, $image);
        }

        return $this->renderForm('talks/new.html.twig',
            [
                'form' => $form
            ]
        );
    }
}
